<?php

use yii\db\Migration;

/**
 * Class m240129_094341_create_items_images
 */
class m240129_094341_create_items_images extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%items_images}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items_images}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240129_094341_create_items_images cannot be reverted.\n";

        return false;
    }
    */
}
