<?php

use yii\db\Migration;

/**
 * Class m240130_075311_add_image_foreign_key_to_item_table
 */
class m240130_075311_add_image_foreign_key_to_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%items}}', 'image_id', $this->integer());
        $this->addForeignKey('image_id', '{{%items}}', 'image_id', '{{%items_images}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240130_075311_add_image_foreign_key_to_item_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240130_075311_add_image_foreign_key_to_item_table cannot be reverted.\n";

        return false;
    }
    */
}
