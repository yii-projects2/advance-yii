<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%items}}`.
 */
class m240126_065257_add_create_at_column_update_at_column_created_by_column_update_by_column_status_column_to_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%items}}', 'create_at', $this->dateTime());
        $this->addColumn('{{%items}}', 'update_at', $this->dateTime());
        $this->addColumn('{{%items}}', 'created_by', $this->string());
        $this->addColumn('{{%items}}', 'update_by', $this->string());
        $this->addColumn('{{%items}}', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%items}}', 'create_at');
        $this->dropColumn('{{%items}}', 'update_at');
        $this->dropColumn('{{%items}}', 'created_by');
        $this->dropColumn('{{%items}}', 'update_by');
        $this->dropColumn('{{%items}}', 'status');
    }
}
