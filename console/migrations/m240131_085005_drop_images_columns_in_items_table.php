<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%images_columns_in_items}}`.
 */
class m240131_085005_drop_images_columns_in_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%items}}', 'image_id');
        $this->dropColumn('{{%items}}', 'image_url');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%images_columns_in_items}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
