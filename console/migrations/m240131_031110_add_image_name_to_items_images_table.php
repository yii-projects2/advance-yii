<?php

use yii\db\Migration;

/**
 * Class m240131_031110_add_image_name_to_items_images_table
 */
class m240131_031110_add_image_name_to_items_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%items_images}}', 'image_name', $this->string()->after('id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240131_031110_add_image_name_to_items_images_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240131_031110_add_image_name_to_items_images_table cannot be reverted.\n";

        return false;
    }
    */
}
