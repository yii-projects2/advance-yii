<?php

use yii\db\Migration;

/**
 * Class m240126_022322_items
 */
class m240126_022322_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'price' => $this->double()->notNull(),
            'currency'  => $this->string()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'desc' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{$item}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240126_022322_items cannot be reverted.\n";

        return false;
    }
    */
}
