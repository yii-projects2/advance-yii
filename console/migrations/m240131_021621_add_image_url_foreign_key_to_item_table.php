<?php

use yii\db\Migration;

/**
 * Class m240131_021621_add_image_url_foreign_key_to_item_table
 */
class m240131_021621_add_image_url_foreign_key_to_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $this->addColumn('{{%items}}', 'image_url', $this->string());
        $this->addForeignKey('image_url', '{{%items}}', 'image_url', '{{%items_images}}', 'image');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240131_021621_add_image_url_foreign_key_to_item_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240131_021621_add_image_url_foreign_key_to_item_table cannot be reverted.\n";

        return false;
    }
    */
}
