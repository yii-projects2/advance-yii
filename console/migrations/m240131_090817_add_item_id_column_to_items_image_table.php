<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%items_image}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%item}}`
 */
class m240131_090817_add_item_id_column_to_items_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%items_images}}', 'item_id', $this->integer());

        // creates index for column `item_id`
        $this->createIndex(
            '{{%idx-items_images-item_id}}',
            '{{%items_images}}',
            'item_id'
        );

        // add foreign key for table `{{%item}}`
        $this->addForeignKey(
            '{{%fk-items_image-item_id}}',
            '{{%items_images}}',
            'item_id',
            '{{%items}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item}}`
        $this->dropForeignKey(
            '{{%fk-items_image-item_id}}',
            '{{%items_image}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            '{{%idx-items_image-item_id}}',
            '{{%items_image}}'
        );

        $this->dropColumn('{{%items_image}}', 'item_id');
    }
}
