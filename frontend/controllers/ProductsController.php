<?php
    namespace frontend\controllers;

use common\models\ProductData;
use common\widgets\Alert;
use Yii;
use yii\models\Post;
    use yii\base\InvalidArgumentException;
    use yii\web\BadRequestHttpException;
    use yii\web\Controller;
    use yii\filters\VerbFilter;
    use yii\filters\AccessControl;

    class ProductsController extends Controller{
        // Displays products page
        public function actionProducts(){
            return $this->render('products');
        }
        
        public function actionAddProduct(){
            $model = new ProductData();

            if ($model->load(Yii::$app->request->post()) && $model->save()){
                return $this->redirect(['products', 'id' => $model->id]);
            } else {
                return $this->render('products', [
                    'model' => $model
                ]);
            }
        }
    }

?>