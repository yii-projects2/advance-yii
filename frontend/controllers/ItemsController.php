<?php

namespace frontend\controllers;

use common\models\Items;
use common\models\ItemsImages;
use Yii;
use yii\bootstrap5\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\UploadedFile;

class ItemsController extends Controller
{
    // Displays items page
    public function actionIndex()
    {
        $model = new Items();

        // you need to get onlY VALID STATUS 
        $dataProvider = new ActiveDataProvider([
            'query' => $model::find()->where(['status' => 1]),
            'pagination' => [
                'pageSize' => 8
            ],
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC
                ]
            ]
        ]);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' =>   $dataProvider
        ]);
    }


    /**
     * Create New Record of Item
     */
    public function actionCreate(){
        return 
        $this->render("create");
    }

    public function actionAdd(){

        $model = new Items();
        $currentDate = date('Y-m-d H:i:s');
        $currentUser = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())){
            $model->currency = strtoupper($model->currency);
            $model->create_at = $currentDate;
            $model->update_at = $currentDate;
            $model->created_by = $currentUser;
            $model->update_by = $currentUser;
            $model->status = 1;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'New item has been added!');
                return $this->redirect('index');
            } 
        }
 
        
    }

    public function actionView($id){

        $set_image = new ItemsImages();

        $item_data = Items::find()
        ->where(['id' => $id])->one();
        $image_data = ItemsImages::find()
        ->where(['item_id' => null])->one();

        $item_images = new ActiveDataProvider([
            'query' => $set_image::find()->where(['item_id' => $id]),
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        return $this->render('view', [
            'item_data' => $item_data,
            'image_data' => $image_data,
            'item_images' => $item_images,
            'set_image' => $set_image
        ]);
    }

    public function actionAlter($id){
        $alter_item = Items::find()
        ->where(['id' => $id])->one();

        return $this->render("update", [
            'alter_item' => $alter_item
        ]); 
    }

    /**
     * Update Required there's unique indentity 
     * $id = ? primary
     * 
     * 1. get id from URL
     * 2. use the ID crawl to item model to search record
     * 3. display the record to the form at view file
     * 4. Only allow to edit name , price , description ONLY (model rules)
     * 5. then update to table (Db)
     * 
     */
    public function actionUpdate($id){
        $item = Items::find()->where(['id' => $id])->one();

        $currentDate = date('Y-m-d H:i:s');
        $currentUser = Yii::$app->user->identity->username;

        if ($item->load(Yii::$app->request->post())){
            $item->update_at = $currentDate;
            $item->update_by = $currentUser;
        }


        if ($item->load(Yii::$app->request->post()) && $item->save()) {
            return $this->redirect(['view', 'id' => $item->id]);
        } else {
            return $this->render('update', [
                'model' => $item,
            ]);
        }

        return 
        $this->redirect("index");
    }

    public function actionDelete($id){
        $selected_item = Items::find()
        ->where(['id' => $id])->one();

        $selected_item->status = -1;

        $selected_item->save();

        return $this->redirect('index');

        //delete (soft delete) 
    }
   
    public function actionImage(){
        $model = new ItemsImages();

        $imageData = new ActiveDataProvider([
            'query' => $model::find(),
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        return $this->render('image', [
            'model' => $model,
            'imageData' => $imageData
        ]);
    }

    public function actionUpload($id){
        $model = new ItemsImages();


        if ($model->load(Yii::$app->request->post())){
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            $model->imageFile->saveAs('uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
            $model->image = 'uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension;
            $model->image_name = $model->imageFile->baseName;

            $model->item_id = $id;

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $id]);
            } 
        }
    }

}
