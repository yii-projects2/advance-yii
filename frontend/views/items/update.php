<div>
    <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

    $model = $alter_item;

    $form = ActiveForm::begin([
        'id' => 'update-item-form',
        'action' => '/items/update?id='.Yii::$app->request->get("id")
    ])
    ?>
    <?= $form->field($model, 'name')->textInput()->label('Name') ?>

    <?= $form->field($model, 'price')->textInput()->label('Price') ?>

    <?= $form->field($model, 'desc')->textarea()->label('Description') ?>
    


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Update!', ['class' => 'btn btn-secondary', 'name' => 'update-button']) ?>
        </div>
        <div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>