<div>

    <?php

    use yii\grid\GridView;
    use yii\bootstrap5\Html;

    echo GridView::widget([
        'dataProvider' => $imageData,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id', 
            [
                'attribute' => 'image',
                'format' => 'html',
                'label' => 'Image',
                'value' => function ($data){
                    return Html::img('../' . $data['image'], ['height' => '100px']);
                },
            ],
            'image_name',
            'item_id'
        ]
    ]) ?>

</div>