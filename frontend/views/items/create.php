<div>
    <?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $model = new \common\models\Items();

    $form = ActiveForm::begin([
        'id' => 'add-product-form',
        'action' => '/items/add'
    ])
    ?>
    <?= $form->field($model, 'name')->textInput()->label('Name') ?>

    <?= $form->field($model, 'currency')->textInput(['style' => 'text-transform:uppercase'])->label('Currency') ?>

    <?= $form->field($model, 'price')->textInput()->label('Price') ?>

    <?= $form->field($model, 'quantity')->textInput(['type' => 'number'])->label('Quantity of Items') ?>

    <?= $form->field($model, 'desc')->textarea()->label('Description') ?>



    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Add Product', ['class' => 'btn btn-primary', 'name' => 'add-button']) ?>
        </div>
        <div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>