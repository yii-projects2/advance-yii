<?php

use common\models\User;
use yii\bootstrap5\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

?>

<div>
    <div>
        <?php echo DetailView::widget([
            'model' => $item_data,
            'attributes' => [
                'name', 'currency', 'price', 'quantity',
                [
                    'attribute' => 'desc',
                    'format' => 'html'
                ],
                [
                    'attribute' => 'created_by',
                    'value' => function ($data) {
                        return User::find()->where(['id' => $data->created_by])->one()->username;
                    }
                ],
                'create_at',
                [
                    'attribute' => 'update_by',
                    'value' => function ($data) {
                        return User::find()->where(['id' => $data->update_by])->one()->username;
                    }
                ],
                'update_at'
            ]
        ])
        ?>

        <?php echo GridView::widget([
            'dataProvider' => $item_images,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'image',
                    'format' => 'html',
                    'value' => function ($data){
                        return Html::img('../' . $data['image'], ['height' => '100px']);
                    },
                    'label' => 'Images'
                ],
                'image_name'
            ]
        ])?>

        <?php
        $select_image_form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
            'id' => 'select-image-form',
            'action' => '/items/upload?id=' . Yii::$app->request->get('id')
        ])
        ?>

        <?= $select_image_form->field($set_image, 'imageFile')->fileInput() ?>

        <div class="form-group mt-3">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Add Image', ['class' => 'btn btn-primary', 'name' => 'add-image-button']) ?>
            </div>
            <div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>