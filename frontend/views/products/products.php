<?php

/** @var yii\web\View $this */

namespace app\models;

use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use yii\base\Model;

class Product extends ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->db;
    }

    public static function tableName()
    {
        return '{{product_data}}';
    }
}

$products = Product::find();

$provider = new ActiveDataProvider([
    'query' => $products,
    'pagination' => [
        'pageSize' => 5,
    ],
    'sort' => [
        'defaultOrder' => [
            'id' => SORT_ASC
        ]
    ]
]);



?>


<div class="products-products">
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <div class="card mt-5">
                    <div class="display-6 text-center">
                        <h2>Here is the data table</h2>
                    </div>

                    <?php
                    echo GridView::widget([
                        'dataProvider' => $provider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id', 'date_added',
                            [
                                'label' => 'Clothes Name',
                                'attribute' => 'name',
                                'format' => 'text'
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($provider) {
                                    if ($provider->status == 1) {
                                        return 'Available';
                                    } else if ($provider->status == 0) {
                                        return 'Unavailable';
                                    } else {
                                        return 'Error';
                                    }
                                },
                                'contentOptions' => function ($provider, $key, $index, $column) {
                                    if ($provider->status == 1)
                                        return ['style' => 'background:palegreen'];
                                    else
                                        return ['style' => 'background:red; color: white'];
                                }

                            ],
                            [
                                'attribute' => 'price',
                                'value' => function ($provider) {
                                    return (number_format($provider->price, 2));
                                }
                            ]
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    $model = new \common\models\ProductData();

    $form = ActiveForm::begin([
        'id' => 'add-product-form',
        'action' => '/products/add-product'
    ])
    ?>
    <?= $form->field($model, 'name')->textInput()->label('Name') ?>

    <?= $form->field($model, 'date_added')->textInput()->label('Date Added') ?>

    <?= $form->field($model, 'product_id')->textInput()->label('Enter Product Code') ?>

    <?= $form->field($model, 'price')->textInput()->label('Price') ?>

    <?= $form->field($model, 'status')->dropdownList(
        [
            1 => 'Available',
            0 => 'Unavailable'
        ],
        ['prompt' => 'Select status']
    ) ?>


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Add Product', ['class' => 'btn btn-primary', 'name' => 'add-product-button']) ?>
        </div>
        <div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>