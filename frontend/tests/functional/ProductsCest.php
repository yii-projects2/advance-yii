<?php
    namespace frontend\tests\functional;
    use frontend\tests\FunctionalTester;
    use yii\helpers\Url;

    class AboutCest{
        public function checkProducts(FunctionalTester $I){
            $I->amOnRoute(Url::toRoute('products/products'));
            $I->see('Products', 'h1');
        }

        protected function formParams($name, $date_added, $product_id, $status, $price){
            return [
                'CreateProductForm[name]' => $name,
                'CreateProductForm[date_added]' => $date_added,
                'CreateProductForm[product_id]' => $product_id,
                'CreateProductForm[status]' => $status,
                'CreateProductForm[price]' => $price,
            ];
        }
    }
?>