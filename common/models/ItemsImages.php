<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "items_images".
 *
 * @property int $id
 * @property string $image
 * @property string|null $image_name
 * @property int|null $item_id
 *
 * @property Items $item
 */
class ItemsImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items_images';
    }

    /**
     * {@inheritdoc}
     */

     public $imageFile;

    public function rules()
    {
        return [
            [['image', 'imageFile'], 'required'],
            [['item_id'], 'default', 'value' => null],
            [['item_id'], 'integer'],
            [['image', 'image_name'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Items::class, 'targetAttribute' => ['item_id' => 'id']],
            [['imageFile'], 'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'image_name' => Yii::t('app', 'Image Name'),
            'item_id' => Yii::t('app', 'Item ID'),
        ];
    }

    /**
     * Gets query for [[Item]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::class, ['id' => 'item_id']);
    }
}
