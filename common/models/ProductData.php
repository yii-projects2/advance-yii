<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_data".
 *
 * @property string $name
 * @property string|null $date_added
 * @property string|null $product_id
 * @property bool|null $status
 * @property float|null $price
 * @property int $id
 */
class ProductData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'id'], 'required'],
            [['date_added'], 'safe'],
            [['status'], 'boolean'],
            [['price'], 'number'],
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['product_id'], 'string', 'max' => 10],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'date_added' => Yii::t('app', 'Date Added'),
            'product_id' => Yii::t('app', 'Product ID'),
            'status' => Yii::t('app', 'Status'),
            'price' => Yii::t('app', 'Price'),
            'id' => Yii::t('app', 'ID'),
        ];
    }
}
