<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $currency
 * @property int $quantity
 * @property string|null $desc
 * @property string|null $create_at
 * @property string|null $update_at
 * @property int|null $created_by
 * @property int|null $update_by
 * @property int|null $status
 * @property int|null $image_id
 * @property string|null $image_url
 *
 * @property ItemsImages $image
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'currency', 'quantity'], 'required'],
            [['price'], 'number'],
            [['quantity', 'created_by', 'update_by', 'status', 'image_id'], 'default', 'value' => null],
            [['quantity', 'created_by', 'update_by', 'status', 'image_id'], 'integer'],
            [['desc'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['name', 'currency', 'image_url'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemsImages::class, 'targetAttribute' => ['image_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'currency' => Yii::t('app', 'Currency'),
            'quantity' => Yii::t('app', 'Quantity'),
            'desc' => Yii::t('app', 'Desc'),
            'create_at' => Yii::t('app', 'Create At'),
            'update_at' => Yii::t('app', 'Update At'),
            'created_by' => Yii::t('app', 'Created By'),
            'update_by' => Yii::t('app', 'Update By'),
            'status' => Yii::t('app', 'Status'),
            'image_id' => Yii::t('app', 'Image ID'),
            'image_url' => Yii::t('app', 'Image Url'),
        ];
    }

    /**
     * Gets query for [[Image]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(ItemsImages::class, ['id' => 'image_id']);
    }
}
